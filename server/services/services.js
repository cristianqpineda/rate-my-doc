const axios = require("axios");

const calls = {}

calls.getCities = () => axios.get("https://www.libertycolombia.com.co/cities-autocomplete/0/all")

calls.getSpecialties = (mode = 662, city, query) => axios.get(`https://www.libertycolombia.com.co/specialty-select/${mode}/${city}/${query}`)

calls.getSpecialists = (query, mode = 662, city, specialty) => axios.get(`https://www.libertycolombia.com.co/word-autocomplete/${query}/${mode}/${city}/${specialty}`)

module.exports = calls;
