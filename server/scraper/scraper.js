const puppeteer = require("puppeteer");
const { processDoctors } = require("../utils/scrapeDoctors");

// mode 662 = Póliza de salud, mode 663 = Accidentes personales
module.exports = {
  async scraper(mode = 662, city, specialty) {
    try {
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.goto(
        `https://www.libertycolombia.com.co/zona-cliente/red-medica?pt=${mode}&c=${city}&e=${specialty}`
      );
      const doctorsData = await processDoctors(browser, page);
      return doctorsData;
    } catch (error) {
      console.error(error);
    }
  },
};
