const mongoose = require("mongoose");
const {
  DB: { URL },
} = require("../config/config");

mongoose
  .connect(URL, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Successfully connected to database."))
  .catch((err) => console.log("DB conection error", err));

module.exports = mongoose;
