const router = require("express").Router();
const passport = require("passport");
require("../auth/passport-setup");

router.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile", "email"],
  })
);

router.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/auth/google");
});

router.get("/google/redirect", passport.authenticate("google", {
  successRedirect: "/auth/home",
  failureRedirect: "/auth/google",
  failureFlash: true,
}));

const authCheck = (req, res, next) =>
  !req.user ? res.redirect("/auth/google") : next();

router.get("/home", authCheck, (req, res) => {
  res.send(`Hi ${req.user.user_name}`);
});

module.exports = router;
