const router = require("express").Router();
const { get_doctors } = require("../controllers/controllers");

router.get("/get-doctors", get_doctors);

module.exports = router;
