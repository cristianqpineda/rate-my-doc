const {
  GOOGLE_AUTH: { CLIENT_ID, CLIENT_SECRET },
} = require("../config/config");
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20");
const User = require("../database/models/user-model");

passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.findById(id);
    return done(null, user);
  } catch (error) {
    console.log(error);
  }
});

passport.use(
  new GoogleStrategy(
    {
      clientID: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      callbackURL: "/auth/google/redirect",
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const currentUser = await User.findOne({ google_id: profile.id });
        let newUser;
        if (!currentUser) {
          const user = new User({
            user_name: profile.displayName,
            google_id: profile.id,
          });
          newUser = await user.save();
        }
        return done(null, currentUser || newUser);
      } catch (error) {
        console.log(error);
      }
    }
  )
);
