const { scraper } = require("../scraper/scraper");

const controller = {};

controller.get_doctors = async (req, res) => {
  try {
    const doctors = await scraper(null, 169);
    res.send(doctors);
  } catch (error) {
    console.log(error);
  }
};

module.exports = controller;
