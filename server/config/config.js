require("dotenv").config();

module.exports = {
  DEV: {
    PORT: process.env.DEV_PORT,
  },
  GOOGLE_AUTH: {
    CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
  },
  DB: {
    URL: `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@ratemydoc-5e0mg.mongodb.net/ratemydoc?retryWrites=true&w=majority`,
  },
};
