const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const apiRoutes = require("./routes/api");
const authRoutes = require("./routes/auth");
const mongoose = require("./database/mongoose-connection");
const passport = require("passport")
const cookieSession = require("cookie-session")
const {
  DEV: { PORT },
} = require("./config/config");
const port = process.env.PORT || PORT;
const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 100,
  keys: ["test"]
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/api/v1", apiRoutes);
app.use("/auth", authRoutes);

app.listen(port, () => console.log(`Server running on port ${port}`));
