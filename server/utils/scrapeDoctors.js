module.exports = {
  async processDoctors(browser, page) {
    try {
      let data = [];

      await (async function processPage(browser, page) {
        try {
          const doctors = await page.$$(".views-form .views-row");
          const nextBtn = await page.$(".pager__item--next > a");

          for (const doctor of doctors) {
            const name = await doctor.$eval("h3", (el) => el.innerText);
            const address = await doctor.$eval(
              ".red__direccion span",
              (el) => el.innerText
            );
            const phone = await doctor.$eval(
              ".red__telefonos",
              (el) => el.innerText
            );
            const services = await doctor.$eval(
              ".red_especialidades .content.is-desktop",
              (el) => el.innerText
            );
            const doctorData = { name, address, phone, services };
            data = [...data, doctorData];
          }

          if (nextBtn) {
            await page.click(".pager__item--next > a");
            await page.waitForSelector(".views-form .views-row");
            await processPage(browser, page);
          } else {
            await browser.close();
          }
        } catch (error) {
          console.error(error);
        }
      })(browser, page);

      return data;
    } catch (error) {
      console.error(error);
    }
  },
};
